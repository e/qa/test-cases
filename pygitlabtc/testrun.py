from gitlab.base import RESTObjectList

from pygitlabtc.linter import InvalidGherkinSyntax
from pygitlabtc.parser import GitlabTestCaseIssue, InvalidSyntax

TEST_RUN_FMT = """
### 🏷 Legend
 * Passed  `[x]`
 * Failed  `[ ]`
 * Skipped `[~]` - it requires to manually edit the description

### 📖 Test plan

{test_plan}
"""

TEST_CASE_FMT = """
- [ ] [#{iid}]({url}) {title}
  <details><summary>scenario</summary>

  ```gherkin
{scenario}
  ```

  </details>
"""


class TestRun:
    def __init__(self, title: str):
        self.title = f"Test Run - {title}"
        self.description = ""

    def test_cases(self, issues: RESTObjectList):
        for tc in issues:
            try:
                scenario = GitlabTestCaseIssue(tc).get_scenario()
            except (InvalidGherkinSyntax, InvalidSyntax):
                continue

            yield TEST_CASE_FMT.format(
                iid=tc.iid,
                url=tc.web_url,
                title=tc.title,
                scenario=self.strip(scenario),
            )

    def strip(self, scenario: str):
        return "\n".join(
            f"  {tc.strip()}"
            for tc in scenario.split("\n")
            if not tc.startswith("Scenario:")
        )

    def run(self, issues: RESTObjectList):
        self.description = TEST_RUN_FMT.format(
            test_plan="".join(self.test_cases(issues))
        )
