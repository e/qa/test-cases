from gitlab.v4.objects.issues import ProjectIssue
from marko import Markdown

from pygitlabtc.linter import GitlabTestCaseGherkinLinter


class InvalidSyntax(Exception):
    pass


class GitlabTestCaseIssue(Markdown):
    def __init__(self, issue: ProjectIssue) -> bool:
        self.issue = issue
        super().__init__()

    def get_scenario(self) -> str:
        if "description" not in self.issue.attributes:
            raise InvalidSyntax("Issue description missing")

        document = self.parse(self.issue.description)
        if len(document.children) < 1:
            raise InvalidSyntax("Empty content")

        # First element is gherkin code block
        elts = [
            e
            for e in document.children
            if e.get_type() == "FencedCode" and e.lang == "gherkin"
        ]
        if len(elts) == 0:
            raise InvalidSyntax("Fenced Gherkin code block missing")
        if len(elts) > 1:
            raise InvalidSyntax("Only one fenced gherkin code block is allowed")

        # Check Gherkin syntax
        elt = elts[0]
        linter = GitlabTestCaseGherkinLinter()
        linter.lint(elt.children[0].children)

        return elt.children[0].children
