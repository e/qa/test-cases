from pathlib import Path
from typing import List

from gherlint.checkers.base_checker import BaseChecker
from gherlint.linter import GherkinLinter, GherkinParser
from gherlint.objectmodel.nodes import Document, Node
from gherlint.registry import CheckerRegistry
from gherlint.reporting import Message, MessageStore, Reporter
from gherlint.walker import ASTWalker
from temppathlib import NamedTemporaryFile


class InvalidGherkinSyntax(Exception):
    pass


class InternalReporter(Reporter):
    MSG_TEMPLATE = "{line}:{column}: {text} ({name})"

    def __init__(self):
        MessageStore().clear()
        super().__init__()
        self.reports = []

    def emit(self, message: Message, node: Node, **format_args) -> None:
        root = node.get_root()
        if not isinstance(root, Document):
            raise RuntimeError(
                "The node passed to add_message does not have "
                "a root parent of type Document."
                "This should never happen if gherlint is used from the command line."
            )
        msg_text = message.text.format(**format_args) if format_args else message.text
        self.reports.append(
            self.MSG_TEMPLATE.format(
                line=node.line,
                column=node.column,
                text=msg_text,
                name=message.name,
            )
        )


class GitlabTestCaseGherkinLinter(GherkinLinter):
    def __init__(self) -> None:
        BaseChecker.__init__(self, reporter=InternalReporter())
        self.checker_registry = CheckerRegistry()
        self.checker_registry.discover()
        self.checkers: List[BaseChecker] = [
            checker(self.reporter) for checker in self.checker_registry
        ]
        self.walker = ASTWalker(self.checkers)

    def lint(self, scenario: str) -> None:
        with NamedTemporaryFile() as tmp:
            tmp.path.write_text(f"Feature: Fake\n{scenario}")
            self.lint_file(tmp.path)

    def lint_file(self, filepath: Path) -> None:
        result = GherkinParser().parse(filepath)
        if result.exception:
            self._handle_parser_error(result)
            raise InvalidGherkinSyntax("\n".join(self.reporter.reports))
        if result.added_language_tag:
            self.reporter.add_message(
                "missing-language-tag",
                result.document,
            )
        elif result.fixed_language_tag:
            self.reporter.add_message(
                "wrong-language-tag",
                result.document,
            )
        self.walker.walk(result.document)

        if self.reporter.reports:
            raise InvalidGherkinSyntax("\n".join(self.reporter.reports))
