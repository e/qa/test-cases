from typing import List, Optional

import gitlab
import typer
from rich.console import Console

from pygitlabtc.linter import InvalidGherkinSyntax
from pygitlabtc.parser import GitlabTestCaseIssue, InvalidSyntax
from pygitlabtc.testrun import TestRun

app = typer.Typer(pretty_exceptions_show_locals=False)


class Command:
    def __init__(self, project):
        self.project = project
        self.console = Console()


class Lint(Command):
    def run(self):
        failed = 0
        issues = self.project.issues.list(iterator=True, issue_type="test_case")

        if len(issues) == 0:
            self.console.print(
                "No test cases in {self.project.name_with_namespace} "
                "or invalid permissions."
            )
            raise typer.Exit(code=1)

        self.console.rule("Lint Test Cases")
        for issue in issues:
            try:
                self.console.print(
                    f"{issue.title} - [link={issue.web_url}]{issue.web_url}[/link]"
                )
                GitlabTestCaseIssue(issue).get_scenario()
            except (InvalidGherkinSyntax, InvalidSyntax) as exn:
                failed = failed + 1
                self.console.print(exn, style="red")

        self.console.rule(
            f"[bold green]{len(issues)} passed[/bold green] - "
            f"[bold red]{failed} failed[/bold red]"
        )
        if failed > 0:
            raise typer.Exit(code=1)


class Create(Command):
    def run(self, title, filter_by, add_label, add_milestone):
        tr = TestRun(title)
        issues = self.project.issues.list(
            iterator=True, issue_type="test_case", labels=filter_by
        )
        tr.run(issues)
        issue = self.project.issues.create(
            {
                "title": tr.title,
                "description": tr.description,
                "labels": add_label,
                "milestone_id": add_milestone,
            }
        )
        self.console.print(
            f"Test run issue created [link={issue.web_url}]{issue.web_url}[/link]"
        )


@app.command()
def lint(
    ctx: typer.Context,
):
    cmd = Lint(ctx.obj)
    cmd.run()


@app.command()
def create(
    ctx: typer.Context,
    title: str = typer.Argument(..., help="Test run title."),
    filter_by: Optional[List[str]] = typer.Option(
        None, help="Filter based on gitlab labels"
    ),
    add_label: Optional[List[str]] = typer.Option(
        None, help="Add label to the Test Run"
    ),
    add_milestone: Optional[int] = typer.Option(
        None, help="Add milestone to the Test Run"
    ),
):
    cmd = Create(ctx.obj)
    cmd.run(title, filter_by, add_label, add_milestone)


@app.callback()
def common(
    ctx: typer.Context,
    project: str,
    token: str = typer.Option(None, envvar="GITLAB_TOKEN"),
    url: str = typer.Option("https://gitlab.e.foundation", help="Gitlab base url."),
):
    gl = gitlab.Gitlab(url, private_token=token)
    try:
        ctx.obj = gl.projects.get(project)
    except gitlab.GitlabGetError as exn:
        typer.echo(exn, err=True)
        raise typer.Exit(code=1)


if __name__ == "__main__":
    app()
