# Test Cases

Global project to share test cases and manage test run.

## Test Plan

* /e/OS [HealthCheck test plan](https://gitlab.e.foundation/e/qa/test-cases/-/quality/test_cases?state=opened&sort=created_desc&page=1&label_name%5B%5D=Test+Plan%3A%3AHealthCheck)

and add a second link to reach the full one like

* /e/OS [Minimal HealthCheck test plan](https://gitlab.e.foundation/e/qa/test-cases/-/quality/test_cases?state=opened&sort=created_desc&page=1&label_name%5B%5D=HealthCheck&label_name%5B%5D=priority%3A%3AP1)
* /e/OS [Full HealtchCheck test plan](https://gitlab.e.foundation/e/qa/test-cases/-/quality/test_cases?state=opened&sort=created_desc&page=1&label_name%5B%5D=HealthCheck)
* /e/OS [Essentials test plan](https://gitlab.e.foundation/e/qa/test-cases/-/quality/test_cases?state=opened&sort=created_desc&page=1&label_name%5B%5D=Essentials)

## Gitlab Test Cases Management

Python library to manage test cases.

* Check Gherkin syntax is used on each test case.
* Create test run issue for a given test plan.
* `TODO` Create junit report from test run issue

## Install

```
pip install pygitlabtc --extra-index-url https://gitlab.e.foundation/api/v4/projects/1362/packages/pypi/simple
```

## Usage

A gitlab token exposed as an environment variable `GITLAB_TOKEN` or command line argument 
is required if you want to create test run or call the linter
on no public projects. Please follow the the gitlab documenation to create your
[personal token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

### Syntax check
* Here a command to lint the test case of a gitlab project.
```
gitlabtc e/qa/test-cases lint
```

### Generating a test run
* Below is the command to create a `Minimal HealthCheck` test plan(to be executed with every version of /e/OS)

```
python3 pygitlabtc/cli.py e/qa/test-cases create "/e/OS v</e/OS version> <Device Name> HealthCheck" --filter-by "HealthCheck" --filter-by "priority::P1" --add-label "test run","<Device Name>" --add-milestone <milestone ID>
```

* Below is the command to create a `Full HealthCheck` test plan(to be executed when we update the firmware of a device, or when we bootstrap/upgrade the device.)

```
python3 pygitlabtc/cli.py e/qa/test-cases create "/e/OS v</e/OS version> <Device Name> HealthCheck Full" --filter-by "HealthCheck" --add-label "test run","<Device Name>" --add-milestone <milestone ID>
```

* Below is the command to create a `Essentials` test plan

```
python3 pygitlabtc/cli.py e/qa/test-cases create "/e/OS v</e/OS version> <Device Name> Essentials" --filter-by "Essentials" --add-label "test run","<Device Name>" --add-milestone <milestone ID>
```

Note : Please adjust the `</e/OS version>`, `<Device Name>` and `<milestone ID>` accordingly
