SCENARIO = """Scenario: test1
  Given abcd
  When azerty
  Then qsdfg
"""
BAD_SCENARIO = """Scenrio: test1
  given abcd
  When azerty
  Then qsdfg
"""

TC = f"""
```gherkin
{SCENARIO}
```
"""
BAD_TC = f"""
```gherkin
{BAD_SCENARIO}
```
"""
