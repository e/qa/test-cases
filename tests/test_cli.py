import gitlab
from typer.testing import CliRunner

from pygitlabtc.cli import app

runner = CliRunner()


def test_invalid_project():
    result = runner.invoke(app, ["e/invalid-project", "lint"])
    assert result.exit_code == 1, result.stdout
    assert "404: 404 Project Not Found\n" in result.stdout


def test_lint():
    result = runner.invoke(app, ["e/qa/test-cases", "lint"])
    assert result.exit_code == 0, result.stdout
    assert "- 0 failed" in result.stdout, result.stdout


def test_create(mocker):
    mocker.patch("gitlab.v4.objects.issues.ProjectIssueManager.create")
    result = runner.invoke(app, ["e/qa/test-cases", "create", "carrier support"])
    assert result.exit_code == 0, result.stdout
    gitlab.v4.objects.issues.ProjectIssueManager.create.assert_called_once()
    attrs = gitlab.v4.objects.issues.ProjectIssueManager.create.call_args.args
    assert len(attrs) == 1
    assert attrs[0]["title"] == "Test Run - carrier support"
    assert "description" in attrs[0]
