import pytest
from core.data import BAD_TC, TC
from gitlab.v4.objects.issues import ProjectIssue

from pygitlabtc.linter import InvalidGherkinSyntax
from pygitlabtc.parser import GitlabTestCaseIssue, InvalidSyntax


def test_GitlabTestCaseIssue(manager):
    tc = GitlabTestCaseIssue(ProjectIssue(manager, attrs={}))
    with pytest.raises(InvalidSyntax, match="Issue description missing"):
        tc.get_scenario()

    tc = GitlabTestCaseIssue(ProjectIssue(manager, attrs={"description": ""}))
    with pytest.raises(InvalidSyntax, match="Empty content"):
        tc.get_scenario()

    tc = GitlabTestCaseIssue(ProjectIssue(manager, attrs={"description": "abcd"}))
    with pytest.raises(InvalidSyntax, match="Fenced Gherkin code block missing"):
        tc.get_scenario()

    tc = GitlabTestCaseIssue(ProjectIssue(manager, attrs={"description": f"{TC}{TC}"}))
    with pytest.raises(
        InvalidSyntax, match="Only one fenced gherkin code block is allowed"
    ):
        tc.get_scenario()

    tc = GitlabTestCaseIssue(ProjectIssue(manager, attrs={"description": BAD_TC}))
    expected_err = r"1:1: Feature has no scenarios \(empty-feature\)"
    with pytest.raises(InvalidGherkinSyntax, match=expected_err):
        tc.get_scenario()

    tc = GitlabTestCaseIssue(ProjectIssue(manager, attrs={"description": TC}))
    assert type(tc.get_scenario()) == str
