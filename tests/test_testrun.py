from core.data import BAD_TC, TC
from gitlab.base import RESTObjectList
from gitlab.v4.objects.issues import ProjectIssue

from pygitlabtc.testrun import TestRun


class MockRESTObjectList(RESTObjectList):
    def __init__(self, issues):
        self.issues = issues
        self.indice = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.indice == len(self.issues):
            raise StopIteration
        issue = self.issues[self.indice]
        self.indice += 1
        return issue


def test_TestRun(manager):
    issues = MockRESTObjectList(
        [
            ProjectIssue(
                manager,
                attrs={
                    "description": TC,
                    "iid": 1,
                    "web_url": "https://tc.com/1",
                    "title": "issue 1",
                },
            ),
            ProjectIssue(
                manager,
                attrs={
                    "description": TC,
                    "iid": 2,
                    "web_url": "https://tc.com/2",
                    "title": "issue 2",
                },
            ),
            ProjectIssue(
                manager,
                attrs={
                    "description": BAD_TC,
                    "iid": 3,
                    "web_url": "https://tc.com/3",
                    "title": "issue 3",
                },
            ),
        ]
    )

    tr = TestRun("carrier support")
    tr.run(issues)
    assert "Test Run - carrier support" == tr.title
    assert "- [ ] [#1]" in tr.description
    assert "- [ ] [#2]" in tr.description
    assert "- [ ] [#3]" not in tr.description
