import pytest
from gitlab.client import Gitlab
from gitlab.v4.objects.issues import ProjectIssueManager

from pygitlabtc.testrun import TestRun

TestRun.__test__ = False


@pytest.fixture
def manager():
    yield ProjectIssueManager(Gitlab())
